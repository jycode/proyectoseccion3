/*
                                                Curso: Fundamentos de C++
                                      Tema: Proyecto 1 (Léxico, Sintaxis, Semántica)
                                                   Creado por Jycode
 
                                              Katya Cornejo | Zandor Sánchez
 */

#include <iostream>

bool verificar(int edadP){
    if(edadP >= 18){
        return true;
    }else{
        return false;
    }
}

int main() {
    
    if (verificar(20)){
        std::cout << "Puede entrar\n";
    }else{
        std::cout << "No puede entrar\n";
    }
    
    return 0;
}



